struct Weather: Codable {
    var title: String
    var description: String
    var created: String
    var temp: String
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let query = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .query)
        created = try query.decode(String.self, forKey: .created)
        let results = try query.nestedContainer(keyedBy: CodingKeys.self, forKey: .results)
        let channel = try results.nestedContainer(keyedBy: CodingKeys.self, forKey: .channel)
        title = try channel.decode(String.self, forKey: .title)
        description = try channel.decode(String.self, forKey: .description)
        
        let item = try channel.nestedContainer(keyedBy: CodingKeys.self, forKey: .item)
        let condition = try item.nestedContainer(keyedBy: CodingKeys.self, forKey: .condition)
        
        temp = try condition.decode(String.self, forKey: .temp)
    }
    
    enum CodingKeys: String, CodingKey {
        case query = "query"
        case results = "results"
        case channel = "channel"
        case created = "created"
        case title = "title"
        case description = "description"
        case item = "item"
        case condition = "condition"
        case temp = "temp"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        var query = container.nestedContainer(keyedBy: CodingKeys.self, forKey: .query)
        
        try query.encode(created, forKey: .created)
    
        var results = query.nestedContainer(keyedBy: CodingKeys.self, forKey: .results)
        var channel = results.nestedContainer(keyedBy: CodingKeys.self, forKey: .channel)
        
        try channel.encode(title, forKey: .title)
        try channel.encode(description, forKey: .description)
    }
}


