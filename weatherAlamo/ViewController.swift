import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.red
        
        easyCall()
    }
    
    func easyCall() {
        let location = "barcelona"
        let url: String = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22\(location)%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"
        
        
        Alamofire.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...299:
                        print("example success")
                    default:
                        print("error with response status: \(status)")
                    }
                }
                //to get JSON return value
                if let result = response.result.value {
                    let JSON = result as! NSDictionary
                    print(JSON)
                    do {
                        let decoder = JSONDecoder()
                        let weather = try decoder.decode(Weather.self, from: response.data!)
                        print("---------------> \(weather.title)")
                    } catch let err {
                        print("Err", err)
                    }
                }
                
        }
        
    }
    

}

